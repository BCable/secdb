<?
#
# SecDB
#
# Author: Brad Cable
# Email: brad@bcable.net
# License: Modified BSD
# License Details:
# http://bcable.net/license.php
#

include_once(dirname(__FILE__).'/secdb_main.php');

class secdb_mysql extends secdb_main{

	var $link;
	var $ret;

	function secdb_mysql($server, $db=null, $username=null, $password=null){
		$this->connect($server,$db,$username,$password);
	}

	function connect($server, $db=null, $username=null, $password=null){
		if($password==null){
			if($username==null) $this->link=mysql_connect($server);
			else $this->link=mysql_connect($server,$username);
		}
		else $this->link=mysql_connect($server,$username,$password);
		if($db!=null) $this->selectdb($db);
	}

	function selectdb($db){
		mysql_select_db($db);
	}

	function query($arg1){
		if(is_array($arg1))
			$args=$arg1;
		else
			$args=func_get_args();

		$query=$this->parse_query($args);
		$this->ret=mysql_query($query,$this->link);
		return $this->ret;
	}

	function query_exec(){
		$query=$this->parse_query(func_get_args());
		return mysql_query($query,$this->link);
	}

	function query_onerow(){
		$query=$this->parse_query(func_get_args());
		$ret=mysql_query($query,$this->link);
		return $this->fetch_assoc($ret);
	}

	function query_onefield(){
		$query=$this->parse_query(func_get_args());
		$ret=mysql_query($query,$this->link);
		return $this->fetch_onefield($ret);
	}

	function fetch_onefield($ret=null){
		if($ret!=null) $arr=mysql_fetch_array($ret);
		else $arr=mysql_fetch_array($this->ret);
		return $arr[0];
	}

	function fetch_assoc($ret=null){
		if($ret==null) $ret=$this->ret;
		//return mysql_fetch_assoc($ret);
		$fields=mysql_num_fields($ret);
		$table=mysql_field_table($ret,0);
		for($i=1;$i<$fields;$i++){
			if(mysql_field_table($ret,$i)!=$table) break;
		}
		if($i==$fields) return mysql_fetch_assoc($ret);
		else{
			$arr=mysql_fetch_row($ret);
			if($arr===false) return false;
			$row=array();
			for($i=0;$i<count($arr);$i++){
				$row[mysql_field_table($ret,$i).'.'.mysql_field_name($ret,$i)]=
					$arr[$i];
			}
			return $row;
		}
	}

	function fetch($ret=null){
		return $this->fetch_assoc($ret);
	}

	function num_rows($ret=null){
		if($ret!=null) return mysql_num_rows($ret);
		else return mysql_num_rows($this->ret);
	}

	function last_insert_id(){
		return mysql_insert_id();
	}

	function error(){
		return mysql_error();
	}

	function errno(){
		return mysql_errno();
	}

	function close($link=null){
		if($link!==null) return mysql_close($link);
		else return mysql_close($this->link);
	}

} ?>
