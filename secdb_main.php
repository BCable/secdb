<?

#
# SecDB
#
# Author: Brad Cable
# Email: brad@bcable.net
# License: Modified BSD
# License Details:
# http://bcable.net/license.php
#

set_magic_quotes_runtime(false);

class secdb_main{

	function query_array(){
		$ret=$this->query(func_get_args());

		$arr=array();
		while($rs=$this->fetch_assoc($ret)){
			$arr[]=$rs;
		}
		return $arr;
	}

	function parse_string($text){
		$text=(get_magic_quotes_gpc()?stripslashes($text):$text);
		$text=str_replace('\'','\'\'',$text);
		return $text;
	}

	function max_length($str,$maxlen){
		if($maxlen==null || $maxlen<1 || strlen($str)<=$maxlen) return $str;
		return substr($str,0,$maxlen);
	}

	function parse_query($args){
		$query=array_shift($args);
		preg_match_all(
			'/==([isq]|sl)(?:\-([0-9]+))?==/',
			$query,
			$matches,
			PREG_OFFSET_CAPTURE
		);
		$offset=0;

		$argsleft=array();

		for($i=0; $i<count($matches[0]); $i++){

			// get current argument
			if(count($argsleft)>0)
				$arg=array_shift($argsleft);
			else{
				$arg=array_shift($args);
				if(is_array($arg)){
					$argsleft=$arg;
					$arg=array_shift($argsleft);
				}
			}

			$type=$matches[1][$i][0];
			if(is_array($matches[2][$i])) $maxlen=$matches[2][$i][0];
			else $maxlen=null;
			//if($arg===null) break;

			switch($type){

				// INTEGER
				case 'i':
					$arg=secdb_main::max_length(intval($arg),$maxlen);
					break;

				// STRING
				case 's':
					$arg=
						'\''.
						secdb_main::max_length(
							secdb_main::parse_string($arg),
							$maxlen
						).
						'\'';
					break;

				// SINGLE-LINE STRING
				case 'sl':
					$arg=
						'\''.
						secdb_main::max_length(
							str_replace(
								"\n",null,
								secdb_main::parse_string($arg)
							),
							$maxlen
						).
						'\'';
					break;

				// QUERY STATEMENT
				case 'q':
					$arg=secdb_main::max_length(
						preg_replace('/^([a-z0-9]*).*$/is','\1',$arg),
						$maxlen
					);
					break;
			}

			$query=substr_replace(
				$query,$arg,$matches[0][$i][1]+$offset,
				strlen($matches[0][$i][0])
			);
			$offset+=strlen($arg)-strlen($matches[0][$i][0]);
		}
		return $query;
	}

	function secparse($text){
		return htmlentities($text);
	}

	function secho($text){
		echo(secdb_main::secparse($text));
	}

	function spreecho($text){
		echo(str_replace("\n",'<br />',secdb_main::secparse($text)));
	}

} ?>
