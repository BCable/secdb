<?

#
# SecDB
#
# Author: Brad Cable
# Email: brad@bcable.net
# License: Modified BSD
# License Details:
# http://bcable.net/license.php
#

include_once(dirname(__FILE__).'/secdb_main.php');

class secdb_sqlite_pdo extends secdb_main{

	var $sqlite_db;
	var $selected_db;

	function __construct($db){
		$this->sqlite_db=new PDO("sqlite:{$db}");
		$this->selected_db=$db;
	}

	function selectdb($db){
		$this->sqlite_db->query("detach database '{$selected_db}'");
		$this->sqlite_db->query("attach database '{$db}'");
		$this->selected_db=$db;
	}

	function query($arg1){
		if(is_array($arg1))
			$args=$arg1;
		else
			$args=func_get_args();

		$query=$this->parse_query($args);
		$this->ret=$this->sqlite_db->query($query);
		return $this->ret;
	}

	function query_exec(){
		$query=$this->parse_query(func_get_args());
		return $this->sqlite_db->exec($query);
	}

	function query_onerow(){
		$query=$this->parse_query(func_get_args());
		$ret=$this->sqlite_db->query($query);
		return $ret->fetch();
	}

	function query_onefield(){
		$query=$this->parse_query(func_get_args());
		$ret=$this->sqlite_db->singleQuery($query);
		if(is_array($ret)) return $ret[0];
		else return $ret;
	}

	function fetch_onefield($ret=null){
		if($ret!=null) $arr=$ret->fetch();
		else $arr=$this->ret->fetch();
		return $arr[0];
	}

	function fetch_assoc($ret=null){
		if($ret!=null) return $ret->fetch();
		else return $this->ret->fetch();
	}

	function fetch($ret=null){
		return $this->fetch_assoc($ret);
	}

	function num_rows($ret=null){
		if($ret!=null) return $ret->rowCount();
		else return $this->ret->rowCount();
	}

	function last_insert_id(){
		return $this->sqlite_db->lastInsertId();
	}

	function error(){
		return $this->sqlite_db->errorInfo();
	}

	function errno(){
		return $this->sqlite_db->errorCode();
	}

} ?>
